package com.marioea.hibernate.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 *creamos la tabla empleado con sus respectivos campos
 */
@Entity
@Table(name = "empleados", schema = "jugueteriahibernate", catalog = "")
public class Empleado {
    private int id;
    private String nif;
    private String nombre;
    private String apellidos;
    private String turno;
    private String direccion;
    private String cargo;
    private String telefono;
    private Encargado encargado;
    private List<Jugueteria> jugueterias;
    private List<JugueteEmpleado> vende;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * generamos el getter de nif
     * @return
     */
    @Basic
    @Column(name = "nif")
    public String getNif() {
        return nif;
    }

    /**
     * generamos el setter de id
     * @param nif
     */
    public void setNif(String nif) {
        this.nif = nif;
    }

    /**
     * generamos el getter de nombre
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * generamos el setter de id
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * generamos el getter de apellidos
     * @return
     */
    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    /**
     * generamos el setters de apellidos
     * @param apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * generamos el getter de turno
     * @return
     */
    @Basic
    @Column(name = "turno")
    public String getTurno() {
        return turno;
    }

    /**
     * generamos el setter de turno
     * @param turno
     */
    public void setTurno(String turno) {
        this.turno = turno;
    }

    /**
     * generamos el getter de direccion
     * @return
     */
    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    /**
     * generamos el setter de direccion
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * generamos el getter de cargo
     * @return
     */
    @Basic
    @Column(name = "cargo")
    public String getCargo() {
        return cargo;
    }

    /**
     * generamos el setter de cargo
     * @param cargo
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * generamos el getter de telefono
     * @return
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * generamos el setter de telefono
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado empleado = (Empleado) o;
        return id == empleado.id &&
                Objects.equals(nif, empleado.nif) &&
                Objects.equals(nombre, empleado.nombre) &&
                Objects.equals(apellidos, empleado.apellidos) &&
                Objects.equals(turno, empleado.turno) &&
                Objects.equals(direccion, empleado.direccion) &&
                Objects.equals(cargo, empleado.cargo) &&
                Objects.equals(telefono, empleado.telefono);
    }
    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nif, nombre, apellidos, turno, direccion, cargo, telefono);
    }

    /**
     * En la siguiente relacion es una relacion entre empleado y encargado seria de tipo (N,1)
     * N seria empleado y 1 encargado en este caso a la hora de mapear se desarrollaria directamente el mapeo
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_encargado", referencedColumnName = "id")
    public Encargado getEncargado() {
        return encargado;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    /**
     * La siguiente relacion que vamos a realizar entre empleados y jugueterias en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto empleados y jugueterias son varios no es solo uno.
     * @return
     */
    @ManyToMany(mappedBy = "empleados")
    public List<Jugueteria> getJugueterias() {
        return jugueterias;
    }

    public void setJugueterias(List<Jugueteria> jugueterias) {
        this.jugueterias = jugueterias;
    }
    /**
     *En esta relación que hacemos es la tabla empleados con la de jugueteempleado en este caso es una relación (1,N)
     *la de empleado seria el 1 a N seria juguete_empleado.Basicamente aqui recogeriamos la relacion que existe entre empleado y juguete_empleado.
     * @return
     */
    @OneToMany(mappedBy = "empleados")
    public List<JugueteEmpleado> getVende() {
        return vende;
    }

    public void setVende(List<JugueteEmpleado> vende) {
        this.vende = vende;
    }
}
