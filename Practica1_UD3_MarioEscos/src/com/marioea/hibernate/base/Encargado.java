package com.marioea.hibernate.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * creamos la tabla de encargado con sus respectivas variables
 */
@Entity
public class Encargado {
    private int id;
    private String nif;
    private String nombre;
    private String apellidos;
    private int anyosCargo;
    private int edad;
    private String telefono;
    private List<Empleado> empleados;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * generamos el getter de nif
     * @return
     */
    @Basic
    @Column(name = "nif")
    public String getNif() {
        return nif;
    }

    /**
     * generamos el setter de nif
     * @param nif
     */
    public void setNif(String nif) {
        this.nif = nif;
    }

    /**
     * generamos el getter de nombre
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * generamos el setter de nombre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * generamos el getter de apellidos
     * @return
     */
    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    /**
     * generamos el setter de apellidos
     * @param apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * generamos el getter de anyos_cargo
     * @return
     */
    @Basic
    @Column(name = "anyos_cargo")
    public int getAnyosCargo() {
        return anyosCargo;
    }

    /**
     * generamos el setter de anyos_cargo
     * @param anyosCargo
     */
    public void setAnyosCargo(int anyosCargo) {
        this.anyosCargo = anyosCargo;
    }

    /**
     * generamos el getter de edad
     * @return
     */
    @Basic
    @Column(name = "edad")
    public int getEdad() {
        return edad;
    }

    /**
     * generamos el setter de edad
     * @param edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * generamos el getter de telefono
     * @return
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * generamos el setter de telefono
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Encargado encargado = (Encargado) o;
        return id == encargado.id &&
                anyosCargo == encargado.anyosCargo &&
                edad == encargado.edad &&
                Objects.equals(nif, encargado.nif) &&
                Objects.equals(nombre, encargado.nombre) &&
                Objects.equals(apellidos, encargado.apellidos) &&
                Objects.equals(telefono, encargado.telefono);
    }
    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nif, nombre, apellidos, anyosCargo, edad, telefono);
    }
    /**
     * En la siguiente relacion es una relacion entre encargado y empleado seria de tipo (1,N)
     * 1,1 seria encargado y 1,N empleado en este caso a la hora de mapear se desarrollaria directamente el mapeo
     * @return
     */
    @OneToMany(mappedBy = "encargado")
    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }
}
