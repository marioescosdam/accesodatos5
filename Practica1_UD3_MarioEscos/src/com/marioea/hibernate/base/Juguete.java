package com.marioea.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * creamos la tabla de juguetes con sus respectivas variables que se encuentran a su vez en la base de datos
 */
@Entity
@Table(name = "juguetes", schema = "jugueteriahibernate", catalog = "")
public class Juguete {
    private int id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private String marca;
    private double precio;
    private Date anyoCreacion;
    private List<ClienteJuguete> comprados;
    private List<Jugueteria> jugueterias;
    private List<JugueteEmpleado> vendidos;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * generamos el getter de codigo
     * @return
     */
    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    /**
     * generamos el setter de codigo
     * @param codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * generamos el getter de nombre
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * generamos el setter de nombre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * generamos el getter de descripcion
     * @return
     */
    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * generamos el setter de descripcion
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * generamos el getter de marca
     * @return
     */
    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    /**
     * generamos el setter de marca
     * @param marca
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * generamos el getter de precio
     * @return
     */
    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    /**
     * generamos el setter de precio
     * @param precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * generamos el getter de anyo_creacion
     * @return
     */
    @Basic
    @Column(name = "anyo_creacion")
    public Date getAnyoCreacion() {
        return anyoCreacion;
    }

    /**
     * generamos el setter de anyo_creacion
     * @param anyoCreacion
     */
    public void setAnyoCreacion(Date anyoCreacion) {
        this.anyoCreacion = anyoCreacion;
    }
    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Juguete juguete = (Juguete) o;
        return id == juguete.id &&
                Double.compare(juguete.precio, precio) == 0 &&
                Objects.equals(codigo, juguete.codigo) &&
                Objects.equals(nombre, juguete.nombre) &&
                Objects.equals(descripcion, juguete.descripcion) &&
                Objects.equals(marca, juguete.marca) &&
                Objects.equals(anyoCreacion, juguete.anyoCreacion);
    }
    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, nombre, descripcion, marca, precio, anyoCreacion);
    }
    /**
     *esta relacion seria la de juguetes con cliente
     * en la cual es una relacion (1,N) y con un atributo que es fecha
     * en el medio de la union, entonces lo hacemos a partes mitad y mitad
     * Hacemos la relacion entre juguetes y compra primero
     * @return
     */
    @OneToMany(mappedBy = "juguetes")
    public List<ClienteJuguete> getComprados() {
        return comprados;
    }

    public void setComprados(List<ClienteJuguete> comprados) {
        this.comprados = comprados;
    }

    /**
     * La siguiente relacion que vamos a realizar entre juguetes y jugueterias en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto juguetes y jugueterias son varios no es solo uno.
     * @return
     */
    @ManyToMany(mappedBy = "juguetes")
    public List<Jugueteria> getJugueterias() {
        return jugueterias;
    }

    public void setJugueterias(List<Jugueteria> jugueterias) {
        this.jugueterias = jugueterias;
    }
    /**
     *esta relacion seria la de juguetes con vendidos
     * en la cual es una relacion (1,N) y con un atributo que es fecha
     * en el medio de la union, entonces lo hacemos a partes mitad y mitad
     * Hacemos la relacion entre juguetes y vendidos primero
     * @return
     */
    @OneToMany(mappedBy = "juguetes")
    public List<JugueteEmpleado> getVendidos() {
        return vendidos;
    }

    public void setVendidos(List<JugueteEmpleado> vendidos) {
        this.vendidos = vendidos;
    }
}
